// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires' x
angular.module('starter.controllers',[]);
angular.module('starter.services',[]);
angular.module('starter', ['ionic','starter.controllers','starter.services','angular-oauth2','ngResource'])
    .constant('appConfig',{
        OAuth:{
            baseUrl: 'http://api.fabiofarias.com.br',
            clientId: 'appid01',
            clientSecret: 'secret',
            grantPath:'/oauth/access_token'
        },
        baseUrl: 'http://api.fabiofarias.com.br',
    })
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}).config(function ($stateProvider,$urlRouterProvider,OAuthProvider,OAuthTokenProvider,appConfig) {

    OAuthProvider.configure(appConfig.OAuth);


    OAuthTokenProvider.configure({
        name: 'token',
        options: {
            secure: false
        }
    });
    $stateProvider
        .state('login',{
            url:'/login',
            templateUrl:'templates/login.html',
            controller: 'LoginController'
        })
        .state('home',{
          url:'/home',
          templateUrl:'templates/home.html',
          controller:'HomeController'
        }).state('logout',{
            url:'/logout',
            templateUrl:'templates/logout.html',
        })
        .state('client',{
          abstract:true,
          url:'/client',
          template:'<ion-nav-view/>'
        })
        .state('client.checkout',{
            cache:false,
            url:'/checkout',
            templateUrl:'templates/client/checkout.html',
            controller:'ClientCheckoutController'
        })
        .state('client.checkout_item_datail',{
          url:'/checkout/detail/:index',
          templateUrl:'templates/client/checkout_item_detail.html',
          controller:'ClientCheckoutDetailController'
        })
        .state('client.checkout_successful',{
            url:'/checkout_succesful',
            templateUrl:'templates/client/checkout_successful.html',
            controller:'ClientCheckoutSuccessful'
        })
        .state('client.view_products',{
          url:'/view_products',
          templateUrl:'templates/client/view_products.html',
          controller:'ClientViewProductController'
      });
    
});
